@extends('layouts.auth.template')

@section('content')
<div class="row justify-content-center mt-5">
    <img src="{{ asset('img/kasir.png') }}" alt="" class="logo">
    <div class="col-md-8 mt-3">
        <form method="POST" action="{{ route('authenticate') }}">
            @csrf
            @method('POST')
            <div class="mb-3 row">
                <label for="email" class="col-md-4 col-form-label text-md-end text-white inter-regular fz-12 text-start">Email</label>
                <div class="col-md-6">
                    <input type="email" class="form-control text-white bg-search inter-regular fz-12 @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="password" class="col-md-4 col-form-label text-md-end text-white inter-regular fz-12 text-start">Password</label>
                <div class="col-md-6">
                    <input type="password" class="form-control text-white bg-search inter-regular fz-12 @error('password') is-invalid @enderror" id="password" name="password">
                    @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col">
                    <input type="submit" class="col-md-3 offset-md-5 btn btn-primary inter-regular fz-12" value="Masuk">
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <span class="inter-regular fz-12 text-white">Silahkan isi data terlebih dahulu untuk melanjutkan ke halaman beranda</span>
    </div>
</div>
@endsection