@extends('layouts.back-office.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Produk
            <small>Manajemen Produk</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-archive"></i> Produk</a></li>
            <li>Tambah</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Produk</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('products.store') }}" method="POST">
                    @csrf
                    {{-- <div class="form-group">
                        <label for="name">Code:</label>
                        <input type="text" name="code" class="form-control" required>
                    </div> --}}
                    <div class="form-group">
                        <label for="name">Nama:</label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                    {{-- <div class="form-group">
                        <label for="description">Deskripsi:</label>
                        <textarea name="description" class="form-control"></textarea>
                    </div> --}}
                    <div class="form-group">
                        <label for="price">Harga:</label>
                        <input type="number" name="price" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="quantity_available">Stok Tersedia:</label>
                        <input type="number" name="quantity_available" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
