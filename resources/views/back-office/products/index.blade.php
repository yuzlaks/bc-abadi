@extends('layouts.back-office.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Produk
            <small>Manajemen Produk</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-archive"></i> Produk</a></li>
        </ol>
    </section>

    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Data Produk</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <a href="{{ route('products.create') }}" class="btn btn-success btn-xs mb-3">Tambah Produk</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            {{-- <th>Code</th> --}}
                            <th>Nama Produk</th>
                            {{-- <th>Deskripsi</th> --}}
                            <th>Harga</th>
                            <th>Stok Tersedia</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key => $product)
                            <tr>
                                <td>{{ ($key + 1) }}</td>
                                {{-- <td>{{ $product->code }}</td> --}}
                                <td>{{ $product->name }}</td>
                                {{-- <td>{{ $product->description }}</td> --}}
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->quantity_available }}</td>
                                <td>
                                    <a href="{{ route('products.edit', $product->id) }}"
                                        class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                    <form action="{{ route('products.destroy', $product->id) }}" method="POST"
                                        style="display: inline;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this product?')"><i class="fa fa-times"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
