@extends('layouts.back-office.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaksi
            <small>Manajemen Transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-archive"></i> Transaksi</a></li>
            <li>Tambah</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Transaksi</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('transactions.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="code">Code:</label>
                        <input type="text" name="code" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="grand_total">Grand Total:</label>
                        <input type="number" name="grand_total" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="status">Status:</label>
                        <input type="number" name="status" class="form-control" required>
                    </div>
                    <hr>
                    <table class="table table-bordered table-striped transaction-table">
                        <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Sub Total</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <button class="btn btn-light add-item" type="button">
                        <i class="fa fa-plus"></i>
                    </button>
                    <button type="submit" class="float-right btn btn-success">Simpan</button>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@push('js')
    <script>
        $('.add-item').click(function(){
            let body_data = `<tr>
                                <td width="50%">
                                    <select class="form-control select2">
                                        <option>A</option>
                                        <option>B</option>
                                        <option>C</option>
                                    </select>
                                </td>
                                <td width="10%"><input type="number" class="form-control"></td>
                                <td width="20%"><input readonly class="form-control"></td>
                                <td width="20%"><input readonly class="form-control"></td>
                                <td>
                                    <button class="btn btn-sm btn-danger delete-item" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </td>
                            </tr>`;

            $('.transaction-table > tbody').append(body_data);
        });

        $('body').on('click', '.delete-item', function(){
            $(this).closest('tr').remove();
        })
    </script>
@endpush