@extends('layouts.back-office.template')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Transaksi
        <small>Manajemen Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-archive"></i> Transaksi</a></li>
        <li>Detail</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Detail Transaksi</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="code">Code:</label>
                <input readonly value="{{ $transaction->code }}" type="text" name="code" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea readonly name="description" class="form-control">{{ $transaction->description }}</textarea>
            </div>
            <div class="form-group">
                <label for="grand_total">Grand Total:</label>
                <input readonly value="{{ $transaction->grand_total }}" type="number" name="grand_total" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="status">Status:</label>
                <input readonly value="{{ $transaction->status }}" type="number" name="status" class="form-control" required>
            </div>
            <hr>
            <table class="table table-bordered table-striped transaction-table">
                <thead>
                    <tr>
                        <th>Produk</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Sub Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transaction->transactionDetail as $items)
                    <tr>
                        <td class="fz-12">{{ $items->product->name }}</td>
                        <td class="fz-12">{{ $items->qty }}</td>
                        <td class="fz-12">Rp {{ number_format($items->product->price, 0) }}</td>
                        <td class="fz-12">Rp {{ number_format($items->sub_total, 0) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection