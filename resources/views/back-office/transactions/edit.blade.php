@extends('layouts.back-office.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaksi
            <small>Manajemen Transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-archive"></i> Transaksi</a></li>
            <li>Edit</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Transaksi</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('transactions.update', $transaction->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="code">Code:</label>
                        <input type="text" name="code" class="form-control" value="{{ $transaction->code }}" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" class="form-control">{{ $transaction->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="grand_total">Grand Total:</label>
                        <input type="number" name="grand_total" class="form-control" value="{{ $transaction->grand_total }}" required>
                    </div>
                    <div class="form-group">
                        <label for="user_id">User ID:</label>
                        <input type="number" name="user_id" class="form-control" value="{{ $transaction->user_id }}" required>
                    </div>
                    <div class="form-group">
                        <label for="status">Status:</label>
                        <input type="number" name="status" class="form-control" value="{{ $transaction->status }}" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
