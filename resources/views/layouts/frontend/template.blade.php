<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dewadev - Kasir</title>
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('back-office/fonts/font-awesome.min.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>

<style>
    @font-face {
        font-family: 'inter-regular';
        src: url('{{ asset('fonts/Inter-Regular.ttf') }}') format('truetype');
    }

    @font-face {
        font-family: 'inter-medium';
        src: url('{{ asset('fonts/Inter-Medium.ttf') }}') format('truetype');
    }

    @font-face {
        font-family: 'inter-semibold';
        src: url('{{ asset('fonts/Inter-SemiBold.ttf') }}') format('truetype');
    }

    .inter-regular {
        font-family: 'inter-regular';
    }

    .inter-medium {
        font-family: 'inter-medium';
    }

    .inter-semibold {
        font-family: 'inter-semibold';
    }

    body {
        background-color: #1a1a1a;
    }

    .icon {
        width: 30px;
    }

    .bg-search {
        border: none !important;
        background-color: #333333 !important;
        padding: 10px !important;
    }

    .bg-search:focus {
        border: none;
        color: #e1e1e1;
        background-color: #333333;
    }

    .rounded-10 {
        border-radius: 10px;
    }

    .push-right {
        float: right !important;
    }

    .fa-shopping-cart {
        margin-right: 10px;
        background-color: #2ed573;
        border-radius: 100%;
        width: 30px;
        height: 30px;
        padding: 6px;
        font-size: 17px;
        color: #212529;
    }

    .fz-12 {
        font-size: 12px;
    }

    .fz-13 {
        font-size: 13px;
    }
    
    .mb-10{
        margin-bottom: 100px !important;
    }

    .avatar{
        border-radius: 100%;
        width: 120px;
        margin: 0 auto;
        margin-bottom: 20px;
    }

    .wm{
        margin-top: 80%;
        width: 40%;
    }
    
    .text-right{
        text-align: right;
    }

    .grand-total-text{
        margin-top: 10px;
        background-color: #20bf6b !important;
        border-radius: 5px;
        padding: 15px;
        text-align: right;
        display: none;
    }

    .mr-x{
        margin-right: 0
    }

    .elem-calc{
        display: none;
    }

    .save-btn{
        display: none;
    }
</style>

<body>
    @yield('content')
    <nav class="navbar bg-dark navbar-expand fixed-bottom">
        <ul class="navbar-nav nav-justified w-100">
            <li class="nav-item">
                <a href="{{ url('home') }}" class="nav-link">
                    <svg width="1.8em" height="1.8em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                        <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
                    </svg>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('create-transaction') }}" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1.8em" height="1.8em" fill="currentColor" class="bi bi-bag-plus" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M8 7.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0v-1.5H6a.5.5 0 0 1 0-1h1.5V8a.5.5 0 0 1 .5-.5"/>
                        <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1m3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1z"/>
                    </svg>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('stock') }}" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1.8em" height="1.8em" fill="currentColor" class="bi bi-archive" viewBox="0 0 16 16">
                        <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5zm13-3H1v2h14zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5"/>
                    </svg>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ url('profile') }}" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1.8em" height="1.8em" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z" />
                    </svg>
                </a>
            </li>
        </ul>
    </nav>
</body>
<script src="{{ asset('back-office/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@stack('js')

</html>