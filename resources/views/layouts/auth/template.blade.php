<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BC Abadi</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<style>
    body {
        background-color: #1a1a1a;
    }

    @font-face {
        font-family: 'inter-regular';
        src: url('{{ asset('fonts/Inter-Regular.ttf') }}') format('truetype');
    }

    @font-face {
        font-family: 'inter-medium';
        src: url('{{ asset('fonts/Inter-Medium.ttf') }}') format('truetype');
    }

    @font-face {
        font-family: 'inter-semibold';
        src: url('{{ asset('fonts/Inter-SemiBold.ttf') }}') format('truetype');
    }

    .inter-regular {
        font-family: 'inter-regular';
    }

    .inter-medium {
        font-family: 'inter-medium';
    }

    .inter-semibold {
        font-family: 'inter-semibold';
    }

    .bg-search {
        border: none !important;
        background-color: #333333 !important;
        padding: 10px !important;
    }

    .bg-search:focus {
        border: none;
        color: #e1e1e1;
        background-color: #333333;
    }

    .rounded-10 {
        border-radius: 10px;
    }

    .fz-12 {
        font-size: 12px;
    }

    .fz-13 {
        font-size: 13px;
    }

    .logo{
        width : 120px;
    }
</style>

<body>
<!-- 
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container">
            <a class="navbar-brand" href="{{ URL('/') }}">BC Abadi</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ms-auto">
                    @guest
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('login') ? 'active' : '' }}"
                                href="{{ route('login') }}">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('register') ? 'active' : '' }}"
                                href="{{ route('register') }}">Register</a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav> -->

    <div class="container">
        @yield('content')
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
    </script>
</body>

</html>
