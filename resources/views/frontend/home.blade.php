@extends('layouts.frontend.template')

@section('content')
<div class="container">
    <div class="row mt-2">
        <h4 class="text-white inter-semibold">Beranda</h4>
    </div>
    <div class="row mt-1">
        <div class="form-group">
            <input type="text" class="form-control bg-search inter-regular text-white" id="searchInput" placeholder="Cari...">
        </div>
    </div>
    <!-- list count -->
    <div class="row mt-3 mb-10">
        <label class="text-white inter-medium">Penjualan Hari Ini (<span id="transactionCount">{{ count($transactions) }}</span>)</label>
        <div class="col">
            @foreach($transactions as $item)
            <div class="card mt-2 bg-dark border-success text-white rounded-10 transaction">
                <div class="card-body inter-semibold">
                    <span>
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> {{ $item->code }}
                    </span>
                    <span class="push-right inter-regular">
                        <b class="text-success">+ Rp {{ number_format($item->grand_total, 0, '.', '.') }}</b>
                    </span>
                    <div class="row mt-3">
                        <div class="col-12">
                            <table class="table table-striped table-dark table-bordered">
                                <thead>
                                    <tr>
                                        <th class="inter-regular fz-12">Produk</th>
                                        <th class="inter-regular fz-12">Qty</th>
                                        <th class="inter-regular fz-12">Harga</th>
                                        <th class="inter-regular fz-12">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($item->transactionDetail as $items)
                                    <tr>
                                        <td class="fz-12">{{ $items->product->name }}</td>
                                        <td class="fz-12 text-right">{{ $items->qty }}</td>
                                        <td class="fz-12 text-right">Rp {{ number_format($items->product->price, 0, '.', '.') }}</td>
                                        <td class="fz-12 text-right">Rp {{ number_format($items->sub_total, 0, '.', '.') }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <span class="inter-regular fz-12">{{ $item->description != "" ? "Deskripsi : ".$item->description : "" }}</span>
                            <a href="{{ url('cetak/'.$item->id) }}"><span class="text-right float-right push-right btn btn-sm fz-12 inter-regular btn-primary">Cetak</span></a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(document).ready(function() {
        // Filter transactions based on input field value
        $('#searchInput').on('input', function() {
            var searchText = $(this).val().toLowerCase();
            $('.transaction').each(function() {
                var transactionCode = $(this).find('.card-body').text().toLowerCase();
                if (transactionCode.includes(searchText)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });

            // Update transaction count
            var visibleTransactions = $('.transaction:visible').length;
            $('#transactionCount').text(visibleTransactions);
        });
    });
</script>
@endpush