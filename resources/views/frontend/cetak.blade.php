<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
</head>

<body>
    <div class="container mt-3">
        <h1 class="text-center fw-bolder">BC ABADI</h1>
        <center>
            <small class="text-center fw-bolder">TOKO CAT & THINNER</small><br>
            <small class="text-center fw-bolder">Jl. Raya Kedung Wonokerto - Prambo</small>
        </center>
        <hr>
        <table class="mt-3">
            <thead>
                <tr>
                    <td>Kode</td>
                    <td>:</td>
                    <th style="text-align: left">{{ $transactions->code }}</th>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <td>:</td>
                    <th style="text-align: left">{{ date('d-m-Y', strtotime($transactions->created_at)) }}</th>
                </tr>
                <tr>
                    <td>Deskripsi</td>
                    <td>:</td>
                    <th style="text-align: left">{{ $transactions->description }}</th>
                </tr>
            </thead>
        </table>
        <table class="table table-striped table-bordered mt-4">
            <thead>
                <tr>
                    <th class="inter-regular fz-12">Produk</th>
                    <th class="inter-regular fz-12">Qty</th>
                    <th class="inter-regular fz-12">Harga</th>
                    <th class="inter-regular fz-12">Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transactions->transactionDetail as $items)
                    <tr>
                        <td class="fz-12">{{ $items->product->name }}</td>
                        <td class="fz-12 text-right">{{ $items->qty }}</td>
                        <td class="fz-12 text-right">Rp {{ number_format($items->product->price, 0, '.', '.') }}</td>
                        <td class="fz-12 text-right">Rp {{ number_format($items->sub_total, 0, '.', '.') }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3">Total</td>
                    <td colspan="4">Rp {{ number_format($transactions->grand_total, 0, '.', '.') }}</td>
                </tr>
            </tbody>
        </table>
        <br><br><br>
    </div>
</body>

</html>
