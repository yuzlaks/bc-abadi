@extends('layouts.frontend.template')

@section('content')
<div class="container">
    <div class="row mt-2">
        <h4 class="text-white inter-semibold">Buat Penjualan</h4>
    </div>
    <div class="row mb-10">
        <div class="col">
            <div class="card bg-dark">
                <div class="card-body inter-regular text-white">
                    <form action="{{ url('store') }}" method="POST">
                        @csrf()
                        <div class="form-group mb-2">
                            <label class="form-label inter-medium fz-13" for="code">Kode <small class="text-secondary">(otomatis)</small></label>
                            <input value="{{ $code }}" readonly type="text" name="code" class="form-control fz-13 text-white inter-regular bg-search" required>
                        </div>
                        <div class="form-group mb-2">
                            <label class="form-label inter-medium fz-13" for="description">Deskripsi <small class="text-secondary">(tidak wajib)</small></label>
                            <textarea name="description" class="form-control fz-13 bg-search text-white inter-regular"></textarea>
                        </div>
                        <div class="detail-transaction"></div>
                        <div class="row">
                            <div class="col">
                                <div class="grand-total-text inter-semibold fz-13"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="button" class="add-btn btn btn-primary btn-sm inter-regular mt-2"><i class="fa fa-plus"></i></button>
                                <button type="button" class="save-btn btn btn-success push-right btn-sm inter-regular mt-2">Selesai</button>
                            </div>
                        </div>
                        <div class="form-group mb-2" style="display: none;">
                            <label class="form-label inter-medium fz-13" for="grand_total">Grand Total <small class="text-secondary">(otomatis)</small></label>
                            <input readonly type="number" name="grand_total" class="form-control fz-13 inter-regular bg-search text-white grand-total" required>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    let product = "<option selected disabled>Pilih Produk</option>";
    $.ajax({
        url: "{{ url('api/product') }}",
        type: "GET",
        success: function(res) {
            $.each(res, function(k, v) {
                product += `<option data-max="${Number(v.quantity_available)}" data-price="${Number(v.price)}" value="${v.id}">${v.name} (${v.quantity_available})</option>`;
            });
        }
    });

    $('.add-btn').click(function() {
        let html = `<div class="row mt-3">
                        <hr>
                        <div class="col-10">
                            <select name="product_id[]" class="form-control select2 choose-product bg-search text-white inter-regular fz-13" id="">
                                ${product}
                            </select>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-sm btn-danger remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                        <div class="col-4 elem-calc">
                            <label class="inter-regular fz-12">Qty</label>
                            <input name="qty[]" data-max="" type="number" class="form-control fz-13 inter-regular bg-search text-white mt-2 qty text-right" placeholder="Qty" value="">
                        </div>
                        <div class="col-4 elem-calc">
                            <label class="inter-regular fz-12">Harga</label>
                            <input name="price[]" readonly type="text" class="form-control fz-13 inter-regular bg-search text-white mt-2 price text-right" placeholder="Harga">
                        </div>
                        <div class="col-4 elem-calc">
                            <label class="inter-regular fz-12">Sub Total</label>
                            <input name="sub_total[]" readonly type="text" class="form-control fz-13 inter-regular bg-search text-white mt-2 sub-total text-right" placeholder="Sub Total">
                        </div>
                    </div>`;

        $('.detail-transaction').append(html);

        $('.select2').select2();
    });

    $('body').on('click', '.remove', function() {
        $(this).closest('.row').remove();
        grandtotal();
    });

    $('body').on('keyup', '.qty', function() {
        // Get the current value of the input field
        let value = $(this).val();

        // Parse the sanitized value as a number
        let parsedValue = parseFloat(value);

        // Check if the parsed value is not a number or less than or equal to 0
        // if (isNaN(parsedValue) || parsedValue <= 0) {
        //     // If the value is not a number or less than or equal to 0, set it to 1
        //     parsedValue = 1;
        // }

        // Set the sanitized value back to the input field
        $(this).val(parsedValue);

        // Proceed with the rest of your logic
        let price = Number($(this).closest('.row').find('.price').val()) || 0;
        let max = $(this).data('max');

        if (parsedValue > max) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "Quantity terlalu besar, sisa stok untuk produk ini adalah " + max
            });

            $(this).val(max);
        } else {
            let sub_total = parsedValue * price;

            $(this).closest('.row').find('.sub-total').val(sub_total.toFixed(0));

            grandtotal();
        }
    });


    $('body').on('select2:select', '.choose-product', function(e) {
        let selectedProductId = $(this).val();
        let selectedOption = $(this).find(':selected');
        let price = selectedOption.data('price');
        let max = selectedOption.data('max');
        if (selectedOption.data('max') <= 0) {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "Stok produk ini sudah habis, silahkan update stok terlebih dahulu"
            });
            $(this).closest('.row').remove();
            grandtotal();
        }else{
            $(this).closest('.row').find('.sub-total').val('');
            $(this).closest('.row').find('.qty').val('');

            // Check if the selected product has already been selected in any other dropdown
            let isDuplicate = $('.choose-product').filter(function() {
                return $(this).val() == selectedProductId;
            }).length > 1;

            if (isDuplicate) {
                // Handle duplicate product selection here (e.g., show an error message)
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Anda sudah memilih produk ini sebelumnya."
                });
                $(this).closest('.row').remove();
                grandtotal();
                $(this).val(null).trigger('change'); // Reset the select2 value
            } else {
                // Retrieve the selected option's data attributes
                $(this).closest('.row').find('.price').val(price);
                // $(this).closest('.row').find('.sub-total').val(price);
                // $(this).closest('.row').find('.qty').val(1);
                $(this).closest('.row').find('.qty').attr('data-max', max);
                $(this).closest('.row').find('.elem-calc').show();
                grandtotal();
            }
        }
    });


    // Event listener for the form submission
    $('.save-btn').click(function(e) {
        e.preventDefault(); // Prevent default form submission behavior
        let grand_total = $('.grand-total-text').text().replace('Total keseluruhan : Rp','');

        // Display SweetAlert2 confirmation dialog
        Swal.fire({
            title: 'Apakah Anda yakin?',
            text: "Total transaksi senilai Rp " + grand_total,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yakin'
        }).then((result) => {
            if (result.isConfirmed) {
                // If user confirms, continue with form submission
                $(this).closest('form').submit();
            }
        });
    });


    function grandtotal() {
        let total = 0;

        $('.detail-transaction > .row').each(function() {
            let subtotal = Number($(this).find('.sub-total').val()) || 0;
            total += subtotal;
        });

        if (total == 0) {
            $('.grand-total-text').hide();
            $('.save-btn').hide();
        } else {
            $('.grand-total-text').show();
            $('.save-btn').show();
        }

        // Update the grand total in your desired location
        $('.grand-total').val(total.toFixed(0));
        $('.grand-total-text').text("Total keseluruhan : Rp " + total.toLocaleString('id-ID'));
    }
</script>
@endpush