@extends('layouts.frontend.template')

@section('content')
<div class="container">
    <div class="row mt-2">
        <h4 class="text-white inter-semibold">Stok</h4>
        <small class="inter-regular text-white fz-12">Sisa stok dan stok barang yang terjual</small>
    </div>
    <div class="row mt-1">
        <div class="form-group">
            <input type="text" class="form-control bg-search inter-regular text-white" id="searchInput" placeholder="Cari...">
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 mt-2">
            <div style="overflow-x: scroll;">
                <table class="table table-striped table-dark table-bordered">
                    <thead>
                        <tr>
                            <th class="inter-regular fz-12">#</th>
                            <th class="inter-regular fz-12">Produk</th>
                            <th class="inter-regular fz-12">Harga</th>
                            <th class="inter-regular fz-12">Stok</th>
                            <th class="inter-regular fz-12">Terjual</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key => $product)
                        <tr>
                            <td class="fz-12">{{ ($key + 1) }}</td>
                            <td class="fz-12">{{ $product->name }}</td>
                            <td class="fz-12">Rp {{ number_format($product->price, 0, '.', '.') }}</td>
                            <td class="fz-12">{{ $product->quantity_available }}</td>
                            <td class="fz-12">{{ $product->quantity_sold }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(document).ready(function() {
        // Filter transactions based on input field value
        $('#searchInput').on('input', function() {
            var searchText = $(this).val().toLowerCase();
            $('table > tbody > tr').each(function() {
                var transactionCode = $(this).find('td').text().toLowerCase();
                if (transactionCode.includes(searchText)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    });
</script>
@endpush