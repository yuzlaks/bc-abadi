@extends('layouts.frontend.template')

@section('content')
<div class="container">
    <div class="row mt-2">
        <h4 class="text-white inter-semibold">Profil</h4>
    </div>
    <div class="row mt-3">
        <img src="{{ asset('img/avatar.webp') }}" class="text-center avatar" alt="">
        <h5 class="text-white inter-medium text-center">{{ ucfirst(Auth::user()->name) }}</h5>
        <small class="inter-medium text-center text-secondary">{{ Auth::user()->email }}</small>
        <div class="col mt-2">
            <center>
                <button class="btn btn-sm btn-primary" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Keluar</button>
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                </form>
            </center>
        </div>
    </div>
</div>
@endsection