<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});

include __DIR__.'/auth.php';
include __DIR__.'/frontend.php';
include __DIR__.'/back-office.php';