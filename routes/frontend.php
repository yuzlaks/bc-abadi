<?php

use App\Http\Controllers\HomeController;

Route::get('home', [HomeController::class, 'index'])->name('home');
Route::get('profile', [HomeController::class, 'profile'])->name('profile');
Route::get('stock', [HomeController::class, 'stock'])->name('stock');
Route::get('create-transaction', [HomeController::class, 'createTransaction']);
Route::post('store', [HomeController::class, 'store']);

Route::get('cetak/{id}', [HomeController::class, 'cetak']);