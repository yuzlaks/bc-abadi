<?php

use App\Http\Controllers\BackOfficeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionController;

Route::get('/admin', [BackOfficeController::class, 'index'])->name('admin');
Route::resource('products', ProductController::class);
Route::resource('transactions', TransactionController::class);