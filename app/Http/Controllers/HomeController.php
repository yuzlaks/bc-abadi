<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\StockOpname;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $transactions = Transaction::with('transactionDetail')->whereDate('created_at', now()->toDateString())->orderBy('id', 'DESC')->get();
        return view('frontend.home', compact('transactions'));
    }

    public function profile()
    {
        return view('frontend.profile');
    }

    public function store(Request $request)
    {
        // 1 : in
        // 2 : out
        
        $transaction = new Transaction();
        $transaction->code = $request->code;
        $transaction->description = $request->description;
        $transaction->grand_total = $request->grand_total;
        $transaction->user_id = Auth::user()->id;
        $transaction->status = 1;
        $transaction->save();

        foreach($request->product_id as $key => $item){
            TransactionDetail::create([
                "transaction_id" => $transaction->id,
                "product_id" => $item,
                "qty" => $request->qty[$key],
                "sub_total" => $request->sub_total[$key],
            ]);

            $product = Product::find($item);

            if ($product) {
                $product->quantity_available -= $request->qty[$key];
                $product->quantity_sold += $request->qty[$key];
                $product->save();
            }
        }

        return redirect('home');
    }

    public function stock()
    {
        $products = Product::orderBy('name','DESC')->get();
        return view('frontend.stock', compact('products'));
    }

    public function cetak($id)
    {
        $transactions = Transaction::with('transactionDetail')->where('id', $id)->first();
        return view('frontend.cetak', compact('transactions'));
    }

    public function createTransaction()
    {
        $transactions = Transaction::whereDate('created_at', now()->toDateString())->count();
        $code = "TR".date('d-m-Y')."-".($transactions + 1);
        return view('frontend.create', compact('code'));
    }
}
