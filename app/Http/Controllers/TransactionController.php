<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::orderBy('id', 'DESC')->get();
        return view('back-office.transactions.index', compact('transactions'));
    }

    public function create()
    {
        return view('back-office.transactions.create');
    }

    public function store(Request $request)
    {
        $post = $request->validate([
            'code' => 'required',
            'grand_total' => 'required|numeric',
            'status' => 'required|integer',
        ]);

        $post["user_id"] = Auth::user()->id;
        $post["description"] = $request->description;

        Transaction::create($post);

        return redirect('transactions')->with('success', 'Transaction created successfully.');
    }

    public function edit(Transaction $transaction)
    {
        return view('back-office.transactions.edit', compact('transaction'));
    }

    public function show($id)
    {
        $transaction = Transaction::with('transactionDetail')->where('transactions.id', $id)->first();
        return view('back-office.transactions.show', compact('transaction'));
    }

    public function update(Request $request, Transaction $transaction)
    {
        $request->validate([
            'code' => 'required',
            'grand_total' => 'required|numeric',
            'user_id' => 'required|integer',
            'status' => 'required|integer',
        ]);

        $transaction->update($request->all());

        return redirect('transactions')->with('success', 'Transaction updated successfully.');
    }

    public function destroy(Transaction $transaction)
    {
        $transaction->delete();

        return redirect('transactions')->with('success', 'Transaction deleted successfully.');
    }
}
