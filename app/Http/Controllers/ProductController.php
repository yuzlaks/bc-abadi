<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->get();
        return view('back-office.products.index', compact('products'));
    }

    public function create()
    {
        return view('back-office.products.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            // 'code' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'quantity_available' => 'required|integer',
        ]);

        Product::create($request->all());

        return redirect('products')->with('success', 'Product berhasil dibuat.');
    }

    public function edit(Product $product)
    {
        return view('back-office.products.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            // 'code' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'quantity_available' => 'required|integer',
        ]);

        $product->update($request->all());

        return redirect('products')->with('success', 'Product berhasil diupdate.');
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect('products')->with('success', 'Product berhasil dihapus.');
    }

    public function getProduct()
    {
        $product = Product::orderBy('name', 'ASC')->get();
        return response()->json($product);
    }
}
